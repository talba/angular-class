import {AngularFire} from 'angularfire2';
import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hello angular 2.0';

  constructor(af:AngularFire ) {
    console.log(af);
  };
}
