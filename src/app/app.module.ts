import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import{AngularFireModule} from 'angularfire2';

const appRoutes : Routes = [
  {path: 'users', component: UsersComponent},
  {path: 'posts', component: PostsComponent},
  {path: '', component: UsersComponent},
  {path: '**', component: PageNotFoundComponent},
];

export const firebaseConfig = {
  apiKey: "AIzaSyA9POLQeFJenN__T9MWeHR8T7SDiXi_NZs",
    authDomain: "angular-a3117.firebaseapp.com",
    databaseURL: "https://angular-a3117.firebaseio.com",
    storageBucket: "angular-a3117.appspot.com",
    messagingSenderId: "196007565624"
}

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
