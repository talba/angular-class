import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class UsersService {

  //private _url = 'http://jsonplaceholder.typicode.com/users';
  private _url = 'http://talbs.myweb.jce.ac.il/api/users';
  usersObservable;

  getUsersFromApi(){
    return this._http.get(this._url).map(res=>res.json());
  }

  getUsers(){
    this.usersObservable = this.af.database.list('/users/').map(
      users =>{
        users.map(
          user => {
            user.postTitles = [];
            for(var p in user.posts){
              user.postTitles.push(
                this.af.database.object('/posts/'+p)
              )
            }
          }
        );
        return users;
      }
    )
    return this.usersObservable;
  }

  addUser(user){
    this.usersObservable.push(user);
    //save the new user into firebase (name and email)
  }

  updateUser(user){
    let userKey = user.$key;
    let userData = {name: user.name, email: user.email};
    this.af.database.object('/users/'+userKey).update(userData);
  }

  deleteUser(user){
    this.af.database.object('/users/'+user.$key).remove()
  }

  constructor(private af:AngularFire, private _http:Http) { }

}
