import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

  users;
  isLoading:Boolean = true;

  currentUser;

  select(user){
    this.currentUser=user;
  }

  constructor(private _userService:UsersService) { }

  addUser(user){
    //this.users.push(user);
    //push insert object to an array
    this._userService.addUser(user);
    //new user goes through form->user component->user service->firebase
  }

  deleteUser(user){
    //this.users.splice(this.users.indexOf(user),1);
    //splice = remove an item from an array. wehere the iten is found in the array and how many.
    this._userService.deleteUser(user);
  }
  
  updateUser(user){
    this._userService.updateUser(user);
  }

  ngOnInit() {
    this._userService.getUsersFromApi().subscribe(usersData => {
      this.users=usersData.result;
      this.isLoading=false;
      console.log(this.users);
    });
    
  }

}
